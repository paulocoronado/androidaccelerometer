package com.example.accellerometer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.List;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    Toolbar toolbar;

    SensorManager sensorManager;
    Sensor sensor;
    TextView txtAceleracion;
    int puntos = 0;


    //Create a graph view
    GraphView graphView;

    //Add a line graph

    LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(new DataPoint[] {});



    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        //Create a graph view
        graphView = findViewById(R.id.graph);
        graphView.addSeries(series);
        graphView.getViewport().setXAxisBoundsManual(true);

        txtAceleracion= findViewById(R.id.txtAceleracion);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> deviceSensors = sensorManager.getSensorList(Sensor.TYPE_ALL);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        //Only integer values
        int x = (int) sensorEvent.values[0];
        int y = (int) sensorEvent.values[1];
        int z = (int) sensorEvent.values[2];

        txtAceleracion.setText("X: " + x + " Y: " + y + " Z: " + z);

        //Calculate the acceleration
        double acceleration = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));

        //Add a data point to the graph
        DataPoint dataPoint = new DataPoint(series.getHighestValueX() + 1, acceleration);
        puntos++;

        series.appendData(dataPoint, true, puntos);

        graphView.getViewport().setMinX(puntos-100);
        graphView.getViewport().setMaxX(puntos);
        //Change the color of the graph
        series.setColor(getResources().getColor(R.color.white));

        //Change the color of the axis
        graphView.getGridLabelRenderer().setHorizontalLabelsColor(getResources().getColor(R.color.white));
        graphView.getGridLabelRenderer().setVerticalLabelsColor(getResources().getColor(R.color.white));

        //Change the color of the grid
        graphView.getGridLabelRenderer().setGridColor(getResources().getColor(R.color.white));





        //Reset the series if the amount of points is greater than 500
        if (puntos > 500) {
            puntos = 0;
            series.resetData(new DataPoint[] {});
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.miCompose:
                //Add a logcat message
                Log.i("MainActivity", "Compose selected");
                return true;
            case R.id.miProfile:
                //Create a logcat message
                Log.i("MainActivity", "Profile selected");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}